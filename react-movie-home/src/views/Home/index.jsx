import React, { useCallback, useEffect } from "react";
import { Grid } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import { useDispatch, useSelector } from "react-redux";
import { fetchMovies } from "../../store/actions/movie";

import MovieItem from "../../components/MovieItem";

const Home = () => {
  const dispatch = useDispatch();
  const movies = useSelector((state) => state.movie.movies);
  const moviePagination = useSelector((state) => state.movie.pagination);

  useEffect(() => {
    if (movies.length === 0) {
      dispatch(fetchMovies({ soTrang: moviePagination.currentPage }));
    }
  }, [dispatch, movies.length, moviePagination.currentPage]);

  const onChangePagination = useCallback(
    (e, page) => {
      dispatch(
        fetchMovies({ soTrang: page }, () => {
          // scoll up to top after selecting a page
          window.scrollTo({
            top: 0,
            behavior: "smooth",
          });
        })
      );
    },
    [dispatch]
  );

  return (
    <>
      <Grid container spacing={4}>
        {movies.map((movie) => {
          return (
            <Grid key={movie.maPhim} item xs={12} sm={6} md={3}>
              <MovieItem movie={movie} />
            </Grid>
          );
        })}
      </Grid>

      {moviePagination && (
        <Pagination
          count={moviePagination.totalPages}
          page={moviePagination.currentPage}
          onChange={onChangePagination}
          variant="outlined"
          shape="rounded"
          style={{ margin: "1.5rem 0" }}
        />
      )}
    </>
  );
};

export default Home;

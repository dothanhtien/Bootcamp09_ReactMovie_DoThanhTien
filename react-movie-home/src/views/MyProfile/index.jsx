import React from "react";
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from "@material-ui/core";
import { useSelector } from "react-redux";

const MyProfile = () => {
  const me = useSelector((state) => state.me || {});

  return (
    <>
      <Typography variant="h4" component="h1" style={{ marginBottom: 32 }}>
        Thông tin tài khoản
      </Typography>
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableBody>
            <TableRow>
              <TableCell>Tài khoản</TableCell>
              <TableCell>{me.taiKhoan}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Họ tên</TableCell>
              <TableCell>{me.hoTen}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Email</TableCell>
              <TableCell>{me.email}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Số điện thoại</TableCell>
              <TableCell>{me.soDT}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Mã nhóm</TableCell>
              <TableCell>{me.maNhom}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Loại người dùng</TableCell>
              <TableCell>{me.maLoaiNguoiDung}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default MyProfile;

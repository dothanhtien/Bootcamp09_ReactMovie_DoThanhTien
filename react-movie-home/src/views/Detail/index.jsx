import React, { useEffect } from "react";
import { Button, Chip, Grid, Typography } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { fetchMovie } from "../../store/actions/movie";
import NotFoundImg from "../../assets/img/notfound.jpg";
import { Link } from "react-router-dom";
import useStyles from "./style";

const Detail = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const movieDetail = useSelector((state) => state.movie.movieDetail || {});

  useEffect(() => {
    dispatch(fetchMovie(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  return (
    <>
      <Button
        variant="contained"
        color="primary"
        component={Link}
        to="/"
        className={classes.backToHomeButton}
      >
        Về trang chủ
      </Button>
      <Grid container spacing={4}>
        <Grid item md={4}>
          <img
            src={movieDetail.hinhAnh}
            alt="movie detail"
            className={classes.image}
            onError={(e) => (e.target.src = NotFoundImg)}
          />
        </Grid>
        <Grid item md={8}>
          <Typography variant="h4" component="h1">
            {movieDetail.tenPhim}
          </Typography>
          <Typography variant="overline">
            Ngày khởi chiếu:{" "}
            {new Date(movieDetail.ngayKhoiChieu).toLocaleDateString()}
          </Typography>
          <div>
            {movieDetail.sapChieu && (
              <Chip
                color="primary"
                label="Sắp chiếu"
                className={classes.primaryChip}
              />
            )}
            {movieDetail.dangChieu && (
              <Chip className={classes.successChip} label="Đang chiếu" />
            )}
            {movieDetail.hot && <Chip color="secondary" label="Hot" />}
          </div>
          <div>
            <Typography variant="overline">
              Đánh giá: {movieDetail.danhGia}/10
            </Typography>
          </div>
          <Typography variant="body2">{movieDetail.moTa}</Typography>
        </Grid>
      </Grid>
    </>
  );
};

export default Detail;

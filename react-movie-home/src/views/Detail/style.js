import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    backToHomeButton: {
      marginBottom: theme.spacing(4),
    },
    image: {
      display: "block",
      width: "100%",
    },
    primaryChip: {
      marginRight: theme.spacing(1),
    },

    successChip: {
      backgroundColor: "#4caf50",
      color: "#ffffff",
      marginRight: theme.spacing(1),
    },
  };
});

export default useStyles;

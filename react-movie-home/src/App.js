import React, { useEffect } from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import { AuthRoute, PrivateRoute } from "./HOCs/Route";
import { useDispatch } from "react-redux";
import { fetchMe } from "./store/actions/auth";

import Home from "./views/Home";
import Detail from "./views/Detail";
import MyProfile from "./views/MyProfile";
import { ACCESS_TOKEN } from "./utils/constants/appConfig";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const token = localStorage.getItem(ACCESS_TOKEN);
    if (token)
      dispatch(
        fetchMe(() => {
          window.location.reload();
        })
      );
  }, [dispatch]);

  return (
    <BrowserRouter>
      <Switch>
        <AuthRoute path="/" exact component={Home} />
        <AuthRoute path="/movies/:id" exact component={Detail} />
        <PrivateRoute
          path="/my-profile"
          exact
          component={MyProfile}
          redirectPath="/"
        />
      </Switch>
    </BrowserRouter>
  );
};

export default App;

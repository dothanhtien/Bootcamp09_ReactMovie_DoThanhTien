import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => {
  return {
    media: {
      maxHeight: 400,
    },
    title: {
      minHeight: 64,
    },
  };
});

export default useStyles;

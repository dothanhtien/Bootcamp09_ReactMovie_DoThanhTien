import React from "react";
import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import { Link } from "react-router-dom";

import NotFoundImg from "../../assets/img/notfound.jpg";
import useStyles from "./style";

const MovieItem = (props) => {
  const classes = useStyles();
  const { maPhim, hinhAnh, tenPhim, moTa, danhGia } = props.movie;

  return (
    <Card>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={hinhAnh}
          component="img"
          onError={(e) => (e.target.src = NotFoundImg)}
          title="movie item"
        />
        <CardContent>
          <Typography
            gutterBottom
            variant="h5"
            component="h2"
            className={classes.title}
          >
            {tenPhim.length > 40 ? tenPhim.substr(0, 36) + " ..." : tenPhim}
          </Typography>
          <Typography variant="overline">Đánh giá: {danhGia}/10</Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {moTa.length > 100 ? moTa.substr(0, 96) + " ..." : moTa}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button
          size="small"
          color="primary"
          component={Link}
          to={`/movies/${maPhim}`}
        >
          Xem chi tiết
        </Button>
      </CardActions>
    </Card>
  );
};

export default MovieItem;

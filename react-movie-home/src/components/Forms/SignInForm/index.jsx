import React, { useCallback, useState } from "react";
import {
  Button,
  CircularProgress,
  TextField,
  Typography,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { signIn } from "../../../store/actions/auth";
import { useEffect } from "react";
import { createAction } from "../../../store/actions";
import { actionType } from "../../../store/actions/type";
import useStyles from "./style";

const SignInForm = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const isSigningIn = useSelector((state) => state.auth.isSigningIn || false);
  const error = useSelector((state) => state.auth.error);
  const [credentials, setCredentials] = useState({ taiKhoan: "", matKhau: "" });

  useEffect(() => {
    return () => {
      dispatch(createAction(actionType.SET_AUTH_ERROR, null));
    };
  }, [dispatch]);

  const handleChange = useCallback(
    (event) => {
      const { name, value } = event.target;
      setCredentials({
        ...credentials,
        [name]: value,
      });
    },
    [setCredentials, credentials]
  );

  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();
      dispatch(
        signIn(credentials, () => {
          props.handleCloseAuthModal();
          history.push("/my-profile");
        })
      );
    },
    [dispatch, credentials, history, props]
  );

  return (
    <form onSubmit={handleSubmit}>
      <TextField
        name="taiKhoan"
        label="Tài khoản"
        type="text"
        fullWidth
        margin="normal"
        onChange={handleChange}
        error={!!error}
      />
      <TextField
        name="matKhau"
        label="Mật khẩu"
        type="password"
        fullWidth
        margin="normal"
        onChange={handleChange}
        error={!!error}
      />
      {error && (
        <Typography
          variant="body1"
          color="error"
          className={classes.errorMessage}
        >
          {error}
        </Typography>
      )}
      <Button
        type="submit"
        variant="contained"
        color="primary"
        fullWidth
        className={classes.signInButton}
        disabled={isSigningIn}
      >
        {isSigningIn ? (
          <>
            <CircularProgress size={24} />
            &nbsp;Đang đăng nhập
          </>
        ) : (
          "Đăng nhập"
        )}
      </Button>
      <Button
        type="button"
        color="primary"
        fullWidth
        className={classes.signUpNavigatorButton}
        onClick={props.handleShowSignUpForm}
      >
        Bạn chưa có tài khoản? Đăng ký ngay
      </Button>
    </form>
  );
};

export default SignInForm;

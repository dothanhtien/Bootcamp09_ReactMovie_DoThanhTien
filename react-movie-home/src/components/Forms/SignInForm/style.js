import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    errorMessage: {
      marginTop: theme.spacing(2),
    },
    signInButton: {
      marginTop: theme.spacing(3),
    },
    signUpNavigatorButton: {
      margin: theme.spacing(2, 0),
    },
  };
});

export default useStyles;

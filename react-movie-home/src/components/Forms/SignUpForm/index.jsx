import React, { useCallback, useEffect, useState } from "react";
import {
  Button,
  CircularProgress,
  TextField,
  Typography,
} from "@material-ui/core";
import useStyles from "./style";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { signUp } from "../../../store/actions/auth";
import { signUpUserSchema } from "../../../services/auth";
import { createAction } from "../../../store/actions";
import { actionType } from "../../../store/actions/type";
import { GROUP_ID } from "../../../utils/constants/appConfig";

const SignUpForm = (props) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const isSigningUp = useSelector((state) => state.auth.isSigningUp);
  const error = useSelector((state) => state.auth.error);
  const [success, setSuccess] = useState(false);
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      email: "",
      hoTen: "",
      soDt: "",
      matKhau: "",
      matKhauXacNhan: "",
      maNhom: GROUP_ID,
    },
    validationSchema: signUpUserSchema,
    validateOnMount: true,
    // khởi tạo lỗi, sau khi dùng resetForm formik.isValid vẫn = true
    // fix lỗi sau khi tạo xong nhấn add user lần nữa mà không báo lỗi
    initialErrors: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      matKhauXacNhan: "",
    },
  });

  useEffect(() => {
    return () => {
      dispatch(createAction(actionType.SET_AUTH_ERROR, null));
    };
  }, [dispatch]);

  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();

      setSuccess(false);

      formik.setTouched({
        taiKhoan: true,
        email: true,
        matKhau: true,
        matKhauXacNhan: true,
      });

      if (!formik.isValid) return;

      dispatch(
        signUp(formik.values, () => {
          formik.resetForm();
          setSuccess(true);
        })
      );
    },
    [dispatch, formik]
  );

  return (
    <form onSubmit={handleSubmit}>
      <TextField
        name="taiKhoan"
        value={formik.values.taiKhoan}
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        error={formik.touched.taiKhoan && !!formik.errors.taiKhoan}
        helperText={formik.touched.taiKhoan && formik.errors.taiKhoan}
        label="Tài khoản *"
        type="text"
        fullWidth
        margin="normal"
      />
      <TextField
        name="email"
        value={formik.values.email}
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        error={formik.touched.email && !!formik.errors.email}
        helperText={formik.touched.email && formik.errors.email}
        label="Email *"
        type="email"
        fullWidth
        margin="normal"
      />
      <TextField
        name="hoTen"
        value={formik.values.hoTen}
        onChange={formik.handleChange}
        error={!!formik.errors.hoTen}
        helperText={formik.errors.hoTen}
        label="Họ tên (không bắt buộc)"
        type="text"
        fullWidth
        margin="normal"
      />
      <TextField
        name="soDt"
        value={formik.values.soDt}
        onChange={formik.handleChange}
        error={!!formik.errors.soDt}
        helperText={formik.errors.soDt}
        label="Số điện thoại (không bắt buộc)"
        type="text"
        fullWidth
        margin="normal"
      />
      <TextField
        name="matKhau"
        value={formik.values.matKhau}
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        error={formik.touched.matKhau && !!formik.errors.matKhau}
        helperText={formik.touched.matKhau && formik.errors.matKhau}
        label="Mật khẩu *"
        type="password"
        fullWidth
        margin="normal"
      />
      <TextField
        name="matKhauXacNhan"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        value={formik.values.matKhauXacNhan}
        error={formik.touched.matKhauXacNhan && !!formik.errors.matKhauXacNhan}
        helperText={
          formik.touched.matKhauXacNhan && formik.errors.matKhauXacNhan
        }
        label="Mật khẩu xác nhận *"
        type="password"
        fullWidth
        margin="normal"
      />
      {success && (
        <Typography variant="body1" className={classes.successMessage}>
          Đăng ký thành công!
        </Typography>
      )}
      {error && (
        <Typography
          variant="body1"
          color="error"
          className={classes.errorMessage}
        >
          {error}
        </Typography>
      )}
      <Button
        type="submit"
        variant="contained"
        color="primary"
        fullWidth
        disabled={isSigningUp}
        className={classes.signUpButton}
      >
        {isSigningUp ? (
          <>
            <CircularProgress size={24} />
            &nbsp;Đang xử lý
          </>
        ) : (
          "Đăng ký"
        )}
      </Button>
      <Button
        color="primary"
        fullWidth
        className={classes.signInNavigatorBututon}
        onClick={props.handleShowSignInForm}
      >
        Bạn đã có tài khoản? Đăng nhập ngay
      </Button>
    </form>
  );
};

export default SignUpForm;

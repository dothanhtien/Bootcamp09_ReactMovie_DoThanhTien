import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    successMessage: {
      color: theme.palette.success.main,
      marginTop: theme.spacing(2),
    },
    errorMessage: {
      marginTop: theme.spacing(2),
    },
    signUpButton: {
      marginTop: theme.spacing(3),
    },
    signInNavigatorBututon: {
      margin: theme.spacing(2, 0),
    },
  };
});

export default useStyles;

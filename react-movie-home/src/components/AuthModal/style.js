import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    dialogTitle: {
      padding: theme.spacing(1, 1, 0, 0),
    },
    contentTitle: {
      fontWeight: theme.typography.fontWeightMedium,
    },
  };
});

export default useStyles;

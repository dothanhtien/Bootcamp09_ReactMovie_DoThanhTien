import React, { useCallback, useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  Typography,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import useStyles from "./style";
import SignInForm from "../Forms/SignInForm";
import SignUpForm from "../Forms/SignUpForm";

const AuthModal = (props) => {
  const classes = useStyles();
  const [showSignInForm, setShowSignInForm] = useState(true);
  const [showSignUpForm, setShowSignUpForm] = useState(false);

  const handleShowSignUpForm = useCallback(() => {
    setShowSignInForm(false);
    setShowSignUpForm(true);
  }, []);

  const handleShowSignInForm = useCallback(() => {
    setShowSignInForm(true);
    setShowSignUpForm(false);
  }, []);

  return (
    <Dialog open={props.open} scroll="body" maxWidth="xs">
      <DialogTitle
        id="auth-dialog-title"
        align="end"
        className={classes.dialogTitle}
      >
        <IconButton aria-label="close" onClick={props.handleCloseAuthModal}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Typography
          variant="h4"
          component="h1"
          align="center"
          className={classes.contentTitle}
        >
          CyberMovie
        </Typography>

        {showSignInForm && (
          <SignInForm
            handleShowSignUpForm={handleShowSignUpForm}
            handleCloseAuthModal={props.handleCloseAuthModal}
          />
        )}
        {showSignUpForm && (
          <SignUpForm
            handleShowSignInForm={handleShowSignInForm}
            handleCloseAuthModal={props.handleCloseAuthModal}
          />
        )}
      </DialogContent>
    </Dialog>
  );
};

export default AuthModal;

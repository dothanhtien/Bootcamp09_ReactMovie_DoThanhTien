import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    title: {
      flexGrow: 1,
      color: theme.palette.primary.contrastText,
      textDecoration: "none",
    },
  };
});

export default useStyles;

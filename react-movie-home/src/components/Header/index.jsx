import React from "react";
import {
  AppBar,
  Button,
  Container,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Link, useHistory } from "react-router-dom";
import useStyles from "./style";
import { useDispatch, useSelector } from "react-redux";
import { signOut } from "../../store/actions/auth";

const Header = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const me = useSelector((state) => state.me);

  const handleSignOutBtnClick = () => {
    dispatch(
      signOut(() => {
        if (history.location.pathname === "/my-profile") {
          history.push("/");
        }
      })
    );
  };

  return (
    <>
      <AppBar position="fixed">
        <Container maxWidth="lg">
          <Toolbar disableGutters>
            <Typography
              variant="h6"
              className={classes.title}
              component={Link}
              to="/"
            >
              CyberMovie
            </Typography>
            {/* (!!{} = true) kiểm tra nếu me = {} thì sẽ check trong object có thuộc tính hay không */}
            {!!me && Object.keys(me).length > 0 ? (
              <>
                <Button color="inherit" component={Link} to="/my-profile">
                  Trang cá nhân
                </Button>
                <Button color="inherit" onClick={handleSignOutBtnClick}>
                  Đăng xuất
                </Button>
              </>
            ) : (
              <>
                <Button color="inherit" onClick={props.handleClickSignInButton}>
                  Đăng nhập / Đăng ký
                </Button>
              </>
            )}
          </Toolbar>
        </Container>
      </AppBar>
      <Toolbar />
    </>
  );
};

export default Header;

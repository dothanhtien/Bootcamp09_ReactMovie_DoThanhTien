import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";

import movie from "./reducers/movie";
import auth from "./reducers/auth";
import me from "./reducers/me";
import loading from "./reducers/loading";

const reducer = combineReducers({ movie, auth, me, loading });

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

export default store;

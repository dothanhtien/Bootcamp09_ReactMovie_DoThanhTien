import { actionType } from "../actions/type";

const initialState = {
  movies: [],
  pagination: { currentPage: 1 },
  movieDetail: null,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_MOVIE_LIST: {
      state.movies = payload;
      return { ...state };
    }
    case actionType.SET_MOVIE_PAGINATION: {
      state.pagination = payload;
      return { ...state };
    }
    case actionType.SET_MOVIE_DETAIL: {
      state.movieDetail = payload;
      return { ...state };
    }
    default:
      return state;
  }
};

export default reducer;

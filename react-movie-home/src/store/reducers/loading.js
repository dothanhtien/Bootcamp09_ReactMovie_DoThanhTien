import { actionType } from "../actions/type";

const initialState = {
  isLoading: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_IS_LOADING: {
      state.isLoading = payload;
      return { ...state };
    }
    default:
      return state;
  }
};

export default reducer;

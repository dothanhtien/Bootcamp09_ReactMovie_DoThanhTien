import { authService } from "../../services/index";
import { createAction } from ".";
import { actionType } from "./type";
import { ACCESS_TOKEN } from "../../utils/constants/appConfig";

export const signIn = (credentials, callback) => {
  return async (dispatch) => {
    dispatch(createAction(actionType.SET_AUTH_ERROR, null));
    dispatch(createAction(actionType.SET_IS_SIGNING_IN, true));

    try {
      const res = await authService.signIn(credentials);

      localStorage.setItem(ACCESS_TOKEN, res.data.content.accessToken);

      setTimeout(() => {
        dispatch(createAction(actionType.SET_IS_SIGNING_IN, false));
        dispatch(createAction(actionType.SET_ME, res.data.content));
        if (callback) {
          callback();
        }
      }, 400);
    } catch (err) {
      console.log(err);
      setTimeout(() => {
        dispatch(
          createAction(actionType.SET_AUTH_ERROR, err.response.data.content)
        );
        dispatch(createAction(actionType.SET_IS_SIGNING_IN, false));
      }, 400);
    }
  };
};

export const signOut = (callback) => {
  return (dispatch) => {
    dispatch(createAction(actionType.SET_ME, null));

    localStorage.removeItem(ACCESS_TOKEN);

    if (callback) {
      callback();
    }
  };
};

export const signUp = (data, callback) => {
  return async (dispatch) => {
    dispatch(createAction(actionType.SET_AUTH_ERROR, null));
    dispatch(createAction(actionType.SET_IS_SIGNING_UP, true));

    try {
      await authService.signUp(data);

      setTimeout(() => {
        dispatch(createAction(actionType.SET_IS_SIGNING_UP, false));
        if (callback) {
          callback();
        }
      }, 400);
    } catch (err) {
      console.log(err);
      setTimeout(() => {
        dispatch(
          createAction(actionType.SET_AUTH_ERROR, err.response.data.content)
        );
        dispatch(createAction(actionType.SET_IS_SIGNING_UP, false));
      }, 400);
    }
  };
};

export const fetchMe = (callback) => {
  return async (dispatch) => {
    try {
      const res = await authService.fetchMe();

      dispatch(createAction(actionType.SET_ME, res.data.content));
    } catch (err) {
      console.log(err);
      if (err.response.status === 401) {
        localStorage.removeItem(ACCESS_TOKEN);

        if (callback) {
          await callback();
        }
      }
    }
  };
};

import { movieService } from "../../services";
import { createAction } from ".";
import { actionType } from "./type";

export const fetchMovies = (params, callback) => {
  return async (dispatch) => {
    dispatch(createAction(actionType.SET_IS_LOADING, true));
    try {
      const res = await movieService.fetchMovies(params);

      const { items, count, currentPage, totalCount, totalPages } =
        res.data.content;

      dispatch(createAction(actionType.SET_MOVIE_LIST, items));

      dispatch(
        createAction(actionType.SET_MOVIE_PAGINATION, {
          count,
          currentPage,
          totalCount,
          totalPages,
        })
      );

      // kiểm tra nếu có truyền vào callback thì mới gọi
      if (callback) {
        callback();
      }
    } catch (err) {
      console.log(err);
    }

    setTimeout(() => {
      dispatch(createAction(actionType.SET_IS_LOADING, false));
    }, 400);
  };
};

export const fetchMovie = (movieId) => {
  return async (dispatch) => {
    dispatch(createAction(actionType.SET_IS_LOADING, true));

    try {
      const res = await movieService.fetchMovie(movieId);

      dispatch(createAction(actionType.SET_MOVIE_DETAIL, res.data.content));
    } catch (err) {
      console.log(err);
    }

    setTimeout(() => {
      dispatch(createAction(actionType.SET_IS_LOADING, false));
    }, 400);
  };
};

import axios from "axios";
import { DOMAIN, GROUP_ID } from "../utils/constants/appConfig";

class MovieService {
  fetchMovies(params) {
    return axios({
      method: "GET",
      url: `${DOMAIN}/api/QuanLyPhim/LayDanhSachPhimPhanTrang`,
      params: {
        maNhom: GROUP_ID,
        // tenPhim: "",
        soTrang: 1,
        soPhanTuTrenTrang: 12,
        ...params,
      },
    });
  }

  fetchMovie(movieId) {
    return axios({
      method: "GET",
      url: `${DOMAIN}/api/QuanLyPhim/LayThongTinPhim`,
      params: {
        MaPhim: movieId,
      },
    });
  }
}

export default MovieService;

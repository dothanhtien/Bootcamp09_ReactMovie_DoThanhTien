import MovieService from "./movie";
import AuthService from "./auth";

export const movieService = new MovieService();
export const authService = new AuthService();

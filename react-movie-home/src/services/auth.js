import axios from "axios";
import * as yup from "yup";
import { DOMAIN } from "../utils/constants/appConfig";

export const signUpUserSchema = yup.object().shape({
  taiKhoan: yup
    .string()
    .required("Bạn chưa nhập tài khoản")
    .matches(/^[a-zA-Z0-9]+$/, "Tài khoản không được chứa kí tự đặc biệt"),
  email: yup
    .string()
    .required("Bạn chưa nhập email")
    .email("Email không đúng định dạng"),
  soDt: yup.string().matches(/^[0-9]+$/, "Số điện thoại phải là số"),
  matKhau: yup.string().required("Bạn chưa nhập mật khẩu"),
  matKhauXacNhan: yup
    .string()
    .required("Bạn chưa nhập mật khẩu xác nhận")
    .oneOf([yup.ref("matKhau")], "Mật khẩu xác nhận không khớp"),
});

class AuthService {
  signIn(credentials) {
    return axios({
      method: "POST",
      url: `${DOMAIN}/api/QuanLyNguoiDung/DangNhap`,
      data: credentials,
    });
  }

  signUp(data) {
    return axios({
      method: "POST",
      url: `${DOMAIN}/api/QuanLyNguoiDung/DangKy`,
      data,
    });
  }

  fetchMe(token) {
    return axios({
      method: "POST",
      url: `${DOMAIN}/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }
}

export default AuthService;

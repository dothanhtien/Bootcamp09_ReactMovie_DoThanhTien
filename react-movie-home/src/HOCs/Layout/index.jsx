import React, { useCallback, useState } from "react";
import { Container } from "@material-ui/core";
import { useSelector } from "react-redux";

import Header from "../../components/Header";
import AuthModal from "../../components/AuthModal";
import LoadingScreen from "../../components/LoadingScreen";

const Layout = (props) => {
  const isLoading = useSelector((state) => state.loading.isLoading);
  const [showAuthModal, setShowAuthModal] = useState(false);

  const handleClickSignInButton = useCallback(() => {
    setShowAuthModal(true);
  }, []);

  const handleCloseAuthModal = useCallback(() => {
    setShowAuthModal(false);
  }, []);

  return (
    <>
      <Header handleClickSignInButton={handleClickSignInButton} />
      <Container maxWidth="lg" style={{ marginTop: 32 }}>
        {props.children}
      </Container>

      <AuthModal
        open={showAuthModal}
        handleCloseAuthModal={handleCloseAuthModal}
      />

      {isLoading && <LoadingScreen />}
    </>
  );
};

export default Layout;

import React from "react";
import { Redirect, Route } from "react-router-dom";
import Layout from "../Layout";

const createRoute = (condition) => {
  return (props) => {
    const {
      path,
      component: RouteComponent,
      redirectPath,
      ...restProps
    } = props;

    return (
      <Route
        path={path}
        {...restProps}
        render={(routeProps) => {
          if (condition()) {
            return (
              <Layout>
                <RouteComponent {...routeProps} />
              </Layout>
            );
          }
          return <Redirect to={redirectPath} />;
        }}
      />
    );
  };
};

export const AuthRoute = createRoute(() => true);

export const PrivateRoute = createRoute(() =>
  localStorage.getItem("access_token")
);

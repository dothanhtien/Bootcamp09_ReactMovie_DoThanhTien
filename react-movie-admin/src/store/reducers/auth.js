import { actionType } from "../actions/type";

const initialState = {
  isSigningIn: false,
  error: null,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_IS_SIGNING_IN: {
      state.isSigningIn = payload;
      return { ...state };
    }
    case actionType.SET_AUTH_ERROR: {
      state.error = payload;
      return { ...state };
    }
    default:
      return state;
  }
};

export default reducer;

import { actionType } from "../actions/type";

const initialState = {
  users: [],
  pagination: {
    currentPage: 1,
    itemsPerPage: 10,
  },
  userTypes: [],
  userDetail: null,
  error: null,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_USER_LIST: {
      state.users = payload;
      return { ...state };
    }
    case actionType.SET_USER_PAGINATION: {
      const clonePagination = { ...state.pagination, ...payload };
      state.pagination = clonePagination;
      return { ...state };
    }
    case actionType.SET_USER_TYPES: {
      state.userTypes = payload;
      return { ...state };
    }
    case actionType.SET_USER_DETAIL: {
      state.userDetail = payload;
      return { ...state };
    }
    case actionType.SET_USER_ERROR: {
      state.error = payload;
      return { ...state };
    }
    default:
      return state;
  }
};

export default reducer;

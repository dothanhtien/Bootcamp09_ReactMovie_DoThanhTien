import { userService } from "../../services";
import { createAction } from ".";
import { actionType } from "./type";

export const fetchUsers = (params, callback) => {
  return async (dispatch) => {
    dispatch(createAction(actionType.SET_IS_LOADING, true));
    try {
      const res = await userService.fetchUsers(params);

      const { items, count, currentPage, totalCount, totalPages } =
        res.data.content;

      dispatch(createAction(actionType.SET_USER_LIST, items));
      dispatch(
        createAction(actionType.SET_USER_PAGINATION, {
          count,
          currentPage,
          totalCount,
          totalPages,
          itemsPerPage: params.soPhanTuTrenTrang,
        })
      );

      if (callback) callback();
    } catch (err) {
      console.log(err);
    }

    setTimeout(() => {
      dispatch(createAction(actionType.SET_IS_LOADING, false));
    }, 400);
  };
};

export const fetchUserTypes = async (dispatch) => {
  try {
    const res = await userService.fetchUserTypes();

    dispatch(createAction(actionType.SET_USER_TYPES, res.data.content));
  } catch (err) {
    console.log(err);
  }
};

export const createUser = (data, callback) => {
  return async (dispatch) => {
    dispatch(createAction(actionType.SET_USER_ERROR, null));
    try {
      const res = await userService.createUser(data);

      if (res.data.statusCode === 200) {
        if (callback) {
          callback();
        }
      }
    } catch (err) {
      console.log(err);
      if (err.response.status === 500) {
        dispatch(
          createAction(actionType.SET_USER_ERROR, err.response.data.content)
        );
      }
    }
  };
};

export const deleteUser = (username, callback) => {
  return async (dispatch) => {
    try {
      const res = await userService.deleteUser(username);

      if (res.data.statusCode === 200) {
        if (callback) {
          callback();
        }
      }
    } catch (err) {
      console.log(err);
    }
  };
};

export const fetchUser = (username) => {
  return async (dispatch) => {
    dispatch(createAction(actionType.SET_IS_LOADING, true));
    try {
      const res = await userService.fetchUser(username);

      const foundUser = res.data.content.find((user) => {
        return user.taiKhoan === username;
      });

      dispatch(createAction(actionType.SET_USER_DETAIL, foundUser));
    } catch (err) {
      console.log(err);
    }

    setTimeout(() => {
      dispatch(createAction(actionType.SET_IS_LOADING, false));
    }, 400);
  };
};

export const updateUser = (data, callback) => {
  return async (dispatch) => {
    dispatch(createAction(actionType.SET_USER_ERROR, null));
    try {
      const res = await userService.updateUser(data);

      if (res.data.statusCode === 200) {
        if (callback) {
          callback();
        }
      }
    } catch (err) {
      console.log(err);
      if (err.response.status === 500) {
        dispatch(
          createAction(actionType.SET_USER_ERROR, err.response.data.content)
        );
      }
    }
  };
};

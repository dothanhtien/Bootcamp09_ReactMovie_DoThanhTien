import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import me from "./reducers/me";
import user from "./reducers/user";
import auth from "./reducers/auth";
import loading from "./reducers/loading";

const reducer = combineReducers({ me, user, auth, loading });

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

export default store;

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    backButton: {
      marginTop: theme.spacing(2),
    },
    pageHeading: {
      margin: theme.spacing(2, 0),
    },
    card: {
      marginTop: theme.spacing(2),
    },
    formControl: {
      margin: theme.spacing(1),
      width: `calc(50% - ${theme.spacing(1) * 2}px)`,
    },
    serverError: {
      margin: theme.spacing(1),
    },
  };
});

export default useStyles;

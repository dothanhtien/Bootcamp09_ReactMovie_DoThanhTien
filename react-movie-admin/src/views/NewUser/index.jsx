import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import useStyles from "./style";
import { useDispatch, useSelector } from "react-redux";
import { createUser, fetchUserTypes } from "../../store/actions/user";
import { useFormik } from "formik";
import { GROUP_ID } from "../../utils/constants/appConfig";
import { createEditUserSchema } from "../../services/user";

import SuccessModal from "../../components/Modals/Success";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";

const NewUser = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const userTypes = useSelector((state) => state.user.userTypes);
  const error = useSelector((state) => state.user.error);
  const [showSuccessModal, setShowSuccessModal] = useState(false);

  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maNhom: GROUP_ID,
      maLoaiNguoiDung: "KhachHang",
      hoTen: "",
      matKhauXacNhan: "",
    },
    validationSchema: createEditUserSchema,
    validateOnMount: true,
    // khởi tạo lỗi, sau khi dùng resetForm formik.isValid vẫn = true
    // fix lỗi sau khi tạo xong nhấn add user lần nữa mà không báo lỗi
    initialErrors: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      matKhauXacNhan: "",
    },
  });

  useEffect(() => {
    dispatch(fetchUserTypes);

    return () => {
      dispatch(createAction(actionType.SET_USER_ERROR, null));
    };
  }, [dispatch]);

  const handleSubmit = (event) => {
    event.preventDefault();

    formik.setTouched({
      taiKhoan: true,
      matKhau: true,
      email: true,
      soDt: true,
      hoTen: true,
      matKhauXacNhan: true,
    });

    if (!formik.isValid) return;

    dispatch(
      createUser(formik.values, () => {
        formik.resetForm();
        setShowSuccessModal(true);
      })
    );
  };

  const handleCloseSuccessModal = () => {
    setShowSuccessModal(false);
  };

  return (
    <>
      <Button
        variant="contained"
        color="primary"
        component={Link}
        to="/"
        className={classes.backButton}
        startIcon={<ArrowBackIcon />}
      >
        Về trang chủ
      </Button>
      <Typography variant="h4" component="h1" className={classes.pageHeading}>
        Thêm người dùng
      </Typography>
      <Card className={classes.card}>
        <CardContent>
          <form onSubmit={handleSubmit}>
            <TextField
              name="taiKhoan"
              value={formik.values.taiKhoan}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.taiKhoan && !!formik.errors.taiKhoan}
              helperText={formik.touched.taiKhoan && formik.errors.taiKhoan}
              label="Tài khoản"
              placeholder="Nhập tài khoản..."
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              className={classes.formControl}
            />
            <TextField
              name="hoTen"
              value={formik.values.hoTen}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.hoTen && !!formik.errors.hoTen}
              helperText={formik.touched.hoTen && formik.errors.hoTen}
              label="Họ tên"
              placeholder="Nhập họ tên..."
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              className={classes.formControl}
            />
            <TextField
              name="email"
              value={formik.values.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.email && !!formik.errors.email}
              helperText={formik.touched.email && formik.errors.email}
              label="Email"
              placeholder="Nhập email..."
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              className={classes.formControl}
            />
            <TextField
              name="soDt"
              value={formik.values.soDt}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.soDt && !!formik.errors.soDt}
              helperText={formik.touched.soDt && formik.errors.soDt}
              label="Số điện thoại"
              placeholder="Nhập số điện thoại..."
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              className={classes.formControl}
            />

            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel id="demo-simple-select-outlined-label">
                Loại người dùng
              </InputLabel>
              <Select
                name="maLoaiNguoiDung"
                value={formik.values.maLoaiNguoiDung}
                onChange={formik.handleChange}
                label="Loại người dùng"
              >
                {userTypes && <MenuItem value="KhachHang" disabled></MenuItem>}
                {userTypes.map((type) => {
                  return (
                    <MenuItem
                      key={type.maLoaiNguoiDung}
                      value={type.maLoaiNguoiDung}
                    >
                      {type.tenLoai}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>

            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel>Mã nhóm</InputLabel>
              <Select
                name="maNhom"
                value={formik.values.maNhom}
                onChange={formik.handleChange}
                label="Mã nhóm"
              >
                <MenuItem value="GP01">GP01</MenuItem>
                <MenuItem value="GP02">GP02</MenuItem>
                <MenuItem value="GP03">GP03</MenuItem>
                <MenuItem value="GP04">GP04</MenuItem>
                <MenuItem value="GP05">GP05</MenuItem>
                <MenuItem value="GP06">GP06</MenuItem>
                <MenuItem value="GP07">GP07</MenuItem>
                <MenuItem value="GP08">GP08</MenuItem>
                <MenuItem value="GP09">GP09</MenuItem>
                <MenuItem value="GP10">GP10</MenuItem>
              </Select>
            </FormControl>

            <TextField
              name="matKhau"
              value={formik.values.matKhau}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.matKhau && !!formik.errors.matKhau}
              helperText={formik.touched.matKhau && formik.errors.matKhau}
              label="Mật khẩu"
              type="password"
              placeholder="Nhập mật khẩu..."
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              className={classes.formControl}
            />
            <TextField
              name="matKhauXacNhan"
              value={formik.values.matKhauXacNhan}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched.matKhauXacNhan && !!formik.errors.matKhauXacNhan
              }
              helperText={
                formik.touched.matKhauXacNhan && formik.errors.matKhauXacNhan
              }
              label="Mật khẩu xác nhận"
              type="password"
              placeholder="Nhập mật khẩu xác nhận..."
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              className={classes.formControl}
            />

            {error && (
              <Typography
                variant="inherit"
                component="p"
                color="error"
                className={classes.serverError}
              >
                {error}
              </Typography>
            )}

            <Box align="right" marginRight={1} marginTop={1}>
              <Button type="submit" variant="contained" color="primary">
                Thêm người dùng
              </Button>
            </Box>
          </form>
        </CardContent>
      </Card>

      <SuccessModal
        open={showSuccessModal}
        handleCloseSuccessModal={handleCloseSuccessModal}
        message="Thêm người dùng thành công"
      />
    </>
  );
};

export default NewUser;

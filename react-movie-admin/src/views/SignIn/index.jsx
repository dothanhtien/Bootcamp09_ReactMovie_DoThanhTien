import React, { useState } from "react";
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Grid,
  Hidden,
  TextField,
  Typography,
} from "@material-ui/core";
import loginImage from "../../assets/login.svg";
import { useDispatch, useSelector } from "react-redux";
import { signIn } from "../../store/actions/auth";
import { useHistory } from "react-router-dom";
import useStyles from "./style";

const SignIn = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();
  const isSigningIn = useSelector((state) => state.auth.isSigningIn);
  const error = useSelector((state) => state.auth.error);
  const [loginForm, setLoginForm] = useState({
    taiKhoan: "",
    matKhau: "",
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setLoginForm({
      ...loginForm,
      [name]: value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    dispatch(
      signIn(loginForm, () => {
        history.push("/");
      })
    );
  };

  return (
    <Box
      minHeight="100vh"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Container maxWidth="md">
        <Grid container spacing={8} justifyContent="center">
          <Hidden smDown>
            <Grid item md={7}>
              <img
                className={classes.loginImage}
                src={loginImage}
                alt="login"
              />
            </Grid>
          </Hidden>
          <Grid item md={5}>
            <Typography
              variant="h3"
              component="h1"
              align="center"
              className={classes.pageTitle}
            >
              CyberMovie
            </Typography>
            <form onSubmit={handleSubmit}>
              <TextField
                type="text"
                name="taiKhoan"
                label="Tài khoản"
                fullWidth
                margin="normal"
                value={loginForm.taiKhoan || ""}
                onChange={handleChange}
                error={!!error}
              />
              <TextField
                type="password"
                name="matKhau"
                label="Mật khẩu"
                fullWidth
                margin="normal"
                value={loginForm.matKhau || ""}
                onChange={handleChange}
                error={!!error}
              />

              {error && (
                <Typography
                  variant="inherit"
                  component="p"
                  color="error"
                  className={classes.errorMessage}
                >
                  {error}
                </Typography>
              )}

              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                className={classes.signinButton}
                disabled={isSigningIn}
              >
                {isSigningIn ? (
                  <>
                    <CircularProgress size={24} />
                    &nbsp;Đang đăng nhập
                  </>
                ) : (
                  "Đăng nhập"
                )}
              </Button>
            </form>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default SignIn;

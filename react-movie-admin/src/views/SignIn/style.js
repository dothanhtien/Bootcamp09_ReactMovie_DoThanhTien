import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    loginImage: {
      width: "100%",
    },
    pageTitle: {
      fontWeight: theme.typography.fontWeightMedium,
    },
    errorMessage: {
      marginTop: theme.spacing(2),
    },
    signinButton: {
      marginTop: theme.spacing(3),
    },
  };
});

export default useStyles;

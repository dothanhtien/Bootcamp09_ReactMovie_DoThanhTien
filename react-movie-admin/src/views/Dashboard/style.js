import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    formControl: {
      display: "block",
    },
    formLabel: {
      marginLeft: theme.spacing(1),
    },
  };
});

export default useStyles;

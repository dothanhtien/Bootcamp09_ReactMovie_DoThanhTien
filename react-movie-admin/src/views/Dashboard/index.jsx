import React, { useCallback, useEffect, useState } from "react";
import {
  Box,
  Button,
  FormControl,
  MenuItem,
  Select,
  Typography,
} from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { Link } from "react-router-dom";
import useStyles from "./style";
import { useDispatch, useSelector } from "react-redux";
import { fetchUsers } from "../../store/actions/user";

import UserList from "../../components/UserList";
import DeleteUserModal from "../../components/Modals/DeleteUser";

const Dashboard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const userList = useSelector((state) => state.user.users);
  const pagination = useSelector((state) => state.user.pagination);
  const [showDeleteUserModal, setShowDeleteUserModal] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);

  useEffect(() => {
    dispatch(
      fetchUsers({
        soTrang: pagination.currentPage,
        soPhanTuTrenTrang: pagination.itemsPerPage,
      })
    );
  }, [dispatch, pagination.currentPage, pagination.itemsPerPage]);

  const handlePaginationChange = useCallback(
    (event, page) => {
      dispatch(
        fetchUsers(
          { soTrang: page, soPhanTuTrenTrang: pagination.itemsPerPage },
          () => {
            window.scrollTo({
              top: 0,
              behavior: "smooth",
            });
          }
        )
      );
    },
    [dispatch, pagination.itemsPerPage]
  );

  const handleChangeItemsPerPage = useCallback(
    (event) => {
      dispatch(
        fetchUsers(
          {
            soPhanTuTrenTrang: event.target.value,
          },
          () => {
            window.scrollTo({
              top: 0,
              behavior: "smooth",
            });
          }
        )
      );
    },
    [dispatch]
  );

  const handleDeleteUser = useCallback((user) => {
    setSelectedUser(user);
    setShowDeleteUserModal(true);
  }, []);

  const handleDeleteUserSuccessful = useCallback(() => {
    setShowDeleteUserModal(false);
    dispatch(
      fetchUsers({
        soTrang: pagination.currentPage,
        soPhanTuTrenTrang: pagination.itemsPerPage,
      })
    );
  }, [dispatch, pagination.currentPage, pagination.itemsPerPage]);

  const handleCloseDeleteUserModal = useCallback(() => {
    setShowDeleteUserModal(false);
  }, []);

  return (
    <>
      <Box display="flex" marginTop={2} justifyContent="space-between">
        <Typography variant="h4" component="h1">
          Danh sách người dùng
        </Typography>
        <Button
          variant="contained"
          color="primary"
          component={Link}
          to="/users/new"
        >
          <PersonAddIcon />
          &nbsp; Thêm người dùng
        </Button>
      </Box>

      <UserList userList={userList} handleDeleteUser={handleDeleteUser} />

      <Box display="flex" justifyContent="space-between" marginY={2}>
        <Pagination
          count={pagination.totalPages}
          page={pagination.currentPage}
          onChange={handlePaginationChange}
          variant="outlined"
          shape="rounded"
        />
        <FormControl className={classes.formControl}>
          <Select
            name="soPhanTuTrenTrang"
            value={pagination.itemsPerPage}
            onChange={handleChangeItemsPerPage}
          >
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={20}>20</MenuItem>
            <MenuItem value={50}>50</MenuItem>
          </Select>
          <Typography
            variant="inherit"
            component="label"
            className={classes.formLabel}
          >
            phần tử mỗi trang
          </Typography>
        </FormControl>
      </Box>

      <DeleteUserModal
        open={showDeleteUserModal}
        user={selectedUser}
        onClose={handleCloseDeleteUserModal}
        handleDeleteUserSuccessful={handleDeleteUserSuccessful}
      />
    </>
  );
};

export default Dashboard;

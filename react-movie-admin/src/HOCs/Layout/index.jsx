import React from "react";
import { Container } from "@material-ui/core";
import Header from "../../components/Header";
import LoadingScreen from "../../components/LoadingScreen";
import { useSelector } from "react-redux";

const Layout = (props) => {
  const isLoading = useSelector((state) => state.loading.isLoading);

  return (
    <>
      <Header />
      <Container maxWidth="md">{props.children}</Container>
      {isLoading && <LoadingScreen />}
    </>
  );
};

export default Layout;

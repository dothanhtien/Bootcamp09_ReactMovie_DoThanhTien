import React from "react";
import { Redirect, Route } from "react-router-dom";

const createRoute = (condition) => {
  return ({
    path,
    layout: LayoutComponent,
    component: RouteComponent,
    redirectPath,
    ...restProps
  }) => {
    return (
      <Route
        path={path}
        {...restProps}
        render={(routeProps) => {
          if (condition()) {
            if (LayoutComponent) {
              return (
                <LayoutComponent>
                  <RouteComponent {...routeProps} />
                </LayoutComponent>
              );
            }

            return <RouteComponent {...routeProps} />;
          }
          return <Redirect to={redirectPath} />;
        }}
      />
    );
  };
};

export const AuthRoute = createRoute(
  () => !localStorage.getItem("access_token")
);

export const PrivateRoute = createRoute(() =>
  localStorage.getItem("access_token")
);

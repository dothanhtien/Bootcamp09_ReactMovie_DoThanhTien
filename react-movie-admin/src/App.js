import React, { useEffect } from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import { AuthRoute, PrivateRoute } from "./HOCs/Route";
import { useDispatch } from "react-redux";
import { fetchMe } from "./store/actions/auth";
import { ACCESS_TOKEN } from "./utils/constants/appConfig";

import Layout from "./HOCs/Layout";

import SignIn from "./views/SignIn";
import Dashboard from "./views/Dashboard";
import NewUser from "./views/NewUser";
import EditUser from "./views/EditUser";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const token = localStorage.getItem(ACCESS_TOKEN);
    if (token)
      dispatch(
        fetchMe(() => {
          window.location.reload();
        })
      );
  }, [dispatch]);

  return (
    <BrowserRouter>
      <Switch>
        <AuthRoute path="/signin" exact component={SignIn} redirectPath="/" />
        <PrivateRoute
          path="/"
          exact
          component={Dashboard}
          layout={Layout}
          redirectPath="/signin"
        />
        <PrivateRoute
          path="/users/new"
          exact
          component={NewUser}
          layout={Layout}
        />
        <PrivateRoute
          path="/users/:id/edit"
          exact
          component={EditUser}
          layout={Layout}
        />
      </Switch>
    </BrowserRouter>
  );
};

export default App;

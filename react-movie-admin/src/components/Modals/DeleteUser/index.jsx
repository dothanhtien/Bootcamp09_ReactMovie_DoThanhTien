import React, { memo } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import { useDispatch } from "react-redux";
import { deleteUser } from "../../../store/actions/user";

const DeleteUserModal = (props) => {
  const dispatch = useDispatch();

  const handleDeleteUser = () => {
    dispatch(
      deleteUser(props.user.taiKhoan, () => {
        props.handleDeleteUserSuccessful();
      })
    );
  };

  return (
    <>
      {props.user && (
        <Dialog open={props.open} onClose={props.onClose}>
          <DialogTitle>Xóa người dùng</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Bạn xác nhận muốn xóa tài khoản {props.user.taiKhoan}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleDeleteUser} color="secondary">
              Đồng ý
            </Button>
            <Button onClick={props.onClose} color="primary" autoFocus>
              Hủy bỏ
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </>
  );
};

export default memo(DeleteUserModal);

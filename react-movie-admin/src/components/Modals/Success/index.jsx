import React from "react";
import {
  Box,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import useStyles from "./style";

const SuccessModal = ({ open, handleCloseSuccessModal, message }) => {
  const classes = useStyles();

  return (
    <Dialog
      open={open}
      onClose={handleCloseSuccessModal}
      fullWidth
      maxWidth="xs"
    >
      <DialogTitle align="end" className={classes.dialogTitle}>
        <IconButton aria-label="close" onClick={handleCloseSuccessModal}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <DialogContentText align="center">
          <Box marginBottom={2} component="span" display="block">
            <CheckCircleIcon className={classes.successIcon} />
          </Box>
          {message}
        </DialogContentText>
      </DialogContent>
    </Dialog>
  );
};

export default SuccessModal;

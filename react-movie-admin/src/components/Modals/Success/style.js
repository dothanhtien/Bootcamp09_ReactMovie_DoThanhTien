import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => {
  return {
    dialogTitle: {
      padding: theme.spacing(1, 1, 0, 0),
    },
    successIcon: {
      color: theme.palette.success.main,
      fontSize: 64,
    },
  };
});

export default useStyles;

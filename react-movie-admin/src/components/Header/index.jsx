import React from "react";
import {
  AppBar,
  Button,
  Toolbar,
  Typography,
  Container,
} from "@material-ui/core";
import { Link, useHistory } from "react-router-dom";
import useStyles from "./style";
import { useDispatch } from "react-redux";
import { signOut } from "../../store/actions/auth";

const Header = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const handleSignOut = () => {
    dispatch(
      signOut(() => {
        history.push("/signin");
      })
    );
  };

  return (
    <>
      <AppBar position="fixed">
        <Container maxWidth="md">
          <Toolbar disableGutters>
            <Typography
              variant="h6"
              className={classes.title}
              component={Link}
              to="/"
            >
              CyberMovie
            </Typography>
            <Button color="inherit" onClick={handleSignOut}>
              Đăng xuất
            </Button>
          </Toolbar>
        </Container>
      </AppBar>
      <Toolbar />
    </>
  );
};

export default Header;

import React from "react";
import {
  Box,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

const UserList = (props) => {
  return (
    <TableContainer component={Paper} style={{ margin: "1rem 0" }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Tài khoản</TableCell>
            <TableCell align="left">Họ tên</TableCell>
            <TableCell align="left">Email</TableCell>
            <TableCell align="left">Số điện thoại</TableCell>
            <TableCell align="left">Mã nhóm</TableCell>
            <TableCell align="left">Mã loại người dùng</TableCell>
            <TableCell align="left"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.userList.map((item) => (
            <TableRow key={item.taiKhoan}>
              <TableCell component="th" scope="row">
                {item.taiKhoan}
              </TableCell>
              <TableCell align="left">{item.hoTen}</TableCell>
              <TableCell align="left" style={{ wordBreak: "break-all" }}>
                {item.email}
              </TableCell>
              <TableCell align="left">{item.soDt}</TableCell>
              <TableCell align="left">{item.maNhom}</TableCell>
              <TableCell align="left">{item.maLoaiNguoiDung}</TableCell>
              <TableCell align="center">
                <Box display="flex">
                  <IconButton
                    color="primary"
                    size="small"
                    component={Link}
                    to={`/users/${item.taiKhoan}/edit`}
                  >
                    <EditIcon />
                  </IconButton>
                  <IconButton
                    color="secondary"
                    size="small"
                    onClick={() => {
                      props.handleDeleteUser(item);
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </Box>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default UserList;

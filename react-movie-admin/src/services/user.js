import axios from "axios";
import * as yup from "yup";
import { ACCESS_TOKEN, DOMAIN, GROUP_ID } from "../utils/constants/appConfig";

export const createEditUserSchema = yup.object().shape({
  taiKhoan: yup
    .string()
    .required("Bạn chưa nhập tài khoản")
    .matches(/^[a-zA-Z0-9]+$/, "Tài khoản không được chứa kí tự đặc biệt"),
  email: yup
    .string()
    .required("Bạn chưa nhập email")
    .email("Email không đúng định dạng"),
  soDt: yup.string().matches(/^[0-9]+$/, "Số điện thoại phải là số"),
  matKhau: yup.string().required("Bạn chưa nhập mật khẩu"),
  matKhauXacNhan: yup
    .string()
    .required("Bạn chưa nhập mật khẩu xác nhận")
    .oneOf([yup.ref("matKhau")], "Mật khẩu xác nhận không khớp"),
});

class UserService {
  fetchUsers(params) {
    return axios({
      method: "GET",
      url: `${DOMAIN}/api/QuanLyNguoiDung/LayDanhSachNguoiDungPhanTrang`,
      params: {
        MaNhom: GROUP_ID,
        // tuKhoa: "",
        soTrang: 1,
        soPhanTuTrenTrang: 10,
        ...params,
      },
    });
  }

  fetchUserTypes() {
    return axios({
      method: "GET",
      url: `${DOMAIN}/api/QuanLyNguoiDung/LayDanhSachLoaiNguoiDung`,
    });
  }

  createUser(data) {
    return axios({
      method: "POST",
      url: `${DOMAIN}/api/QuanLyNguoiDung/ThemNguoiDung`,
      data,
      headers: {
        Authorization: "Bearer " + localStorage.getItem(ACCESS_TOKEN),
      },
    });
  }

  fetchUser(username) {
    return axios({
      method: "GET",
      url: `${DOMAIN}/api/QuanLyNguoiDung/TimKiemNguoiDung`,
      params: {
        MaNhom: GROUP_ID,
        tuKhoa: username,
      },
    });
  }

  updateUser(data) {
    return axios({
      method: "POST",
      url: `${DOMAIN}/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
      data,
      headers: {
        Authorization: "Bearer " + localStorage.getItem(ACCESS_TOKEN),
      },
    });
  }

  deleteUser(username) {
    return axios({
      method: "DELETE",
      url: `${DOMAIN}/api/QuanLyNguoiDung/XoaNguoiDung`,
      params: {
        TaiKhoan: username,
      },
      headers: {
        Authorization: "Bearer " + localStorage.getItem(ACCESS_TOKEN),
      },
    });
  }
}

export default UserService;

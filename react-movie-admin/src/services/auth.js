import axios from "axios";
import { ACCESS_TOKEN, DOMAIN } from "../utils/constants/appConfig";

class AuthService {
  signIn(credentials) {
    return axios({
      method: "POST",
      url: `${DOMAIN}/api/QuanLyNguoiDung/DangNhap`,
      data: credentials,
    });
  }

  fetchMe() {
    return axios({
      method: "POST",
      url: `${DOMAIN}/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem(ACCESS_TOKEN),
      },
    });
  }
}

export default AuthService;
